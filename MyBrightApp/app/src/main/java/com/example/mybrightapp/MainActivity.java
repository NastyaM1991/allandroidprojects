package com.example.mybrightapp;

import android.content.Context;
import android.content.Intent;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraManager;
import android.media.AudioManager;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.ToggleButton;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity
{
    TextView textViewBrightness, textViewVolume;
    SeekBar seekBarBrightness, seekBarVolume;
    int brightness;
    Context context;
    Button buttonWriteSettingsPermission;

    CameraManager mCameraManager;
    String mCameraId;

    ToggleButton toggleButtonOnOffFlashlight;
    AudioManager mAudioManager;

    WifiManager wifiManager;
    Switch switchWifi;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        context = getApplicationContext();

        buttonWriteSettingsPermission = findViewById(R.id.buttonWriteSettingsPermission);
        buttonWriteSettingsPermission.setOnClickListener(buttonWriteSettingsPermissionOnClick);

        //screen brightness
        textViewBrightness = findViewById(R.id.textViewBrightness);

        seekBarBrightness = findViewById(R.id.seekBarBrightness);

        brightness = Settings.System.getInt(context.getContentResolver(), Settings.System.SCREEN_BRIGHTNESS, 0);
        seekBarBrightness.setProgress(brightness);

        textViewBrightness.setText("Screen Brightness: " + seekBarBrightness.getProgress());
        seekBarBrightness.setOnSeekBarChangeListener(seekBarBrightnessOnSeek);

        //flashlight
        mCameraManager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        try
        {
            mCameraId = mCameraManager.getCameraIdList()[0];
        } catch (CameraAccessException e)
        {
            e.printStackTrace();
        }

        toggleButtonOnOffFlashlight = findViewById(R.id.toggleButtonOnOffFlashlight);
        toggleButtonOnOffFlashlight.setOnCheckedChangeListener(toggleButtonOnOffFlashlightOnChange);

        //volume
        mAudioManager = (AudioManager) getSystemService(AUDIO_SERVICE);

        textViewVolume = findViewById(R.id.textViewVolume);

        seekBarVolume = findViewById(R.id.seekBarVolume);
        seekBarVolume.setOnSeekBarChangeListener(seekBarVolumeOnSeek);

        //wifi
        switchWifi = findViewById(R.id.switchWifi);
        wifiManager = (WifiManager) getApplicationContext().getSystemService(context.WIFI_SERVICE);

        switchWifi.setOnCheckedChangeListener(switchWifiOnChange);
    }

    SeekBar.OnSeekBarChangeListener seekBarBrightnessOnSeek = new SeekBar.OnSeekBarChangeListener()
    {
        @Override
        public void onProgressChanged(SeekBar seekBar, int i, boolean b)
        {
            textViewBrightness.setText("Screen Brightness: " + seekBarBrightness.getProgress());
            Settings.System.putInt(context.getContentResolver(), Settings.System.SCREEN_BRIGHTNESS, i);
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar)
        {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar)
        {

        }
    };

    SeekBar.OnSeekBarChangeListener seekBarVolumeOnSeek = new SeekBar.OnSeekBarChangeListener()
    {
        @Override
        public void onProgressChanged(SeekBar seekBar, int i, boolean b)
        {
            mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, seekBarVolume.getProgress(), AudioManager.FLAG_SHOW_UI);
            textViewVolume.setText("Volume: " + seekBarVolume.getProgress());
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar)
        {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar)
        {

        }
    };

    View.OnClickListener buttonWriteSettingsPermissionOnClick = new View.OnClickListener()
    {
        @RequiresApi(api = Build.VERSION_CODES.M)
        @Override
        public void onClick(View view)
        {
            Context context = getApplicationContext();

            boolean settingsCanWrite = Settings.System.canWrite(context);

            if (!settingsCanWrite)
            {
                // If do not have write settings permission then open the Can modify system settings panel.
                Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS);
                startActivity(intent);
            }
            else
            {
                // If has permission then show an alert dialog with message.
                AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
                alertDialog.setMessage("You have system write settings permission now.");
                alertDialog.show();
            }
        }
    };

    ToggleButton.OnCheckedChangeListener toggleButtonOnOffFlashlightOnChange = new CompoundButton.OnCheckedChangeListener()
    {
        @RequiresApi(api = Build.VERSION_CODES.M)
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b)
        {
            switchFlashLight(b);
        }
    };

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void switchFlashLight(boolean status)
    {
        try
        {
            mCameraManager.setTorchMode(mCameraId, status);
        } catch (CameraAccessException e)
        {
            e.printStackTrace();
        }
    }


    Switch.OnCheckedChangeListener switchWifiOnChange = new CompoundButton.OnCheckedChangeListener()
    {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b)
        {
            if (b)
            {
                wifiManager.setWifiEnabled(true);
            }
            else
            {
                wifiManager.setWifiEnabled(false);
            }
        }
    };
}
