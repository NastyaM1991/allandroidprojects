package com.example.mylayoutapp;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity
{
    EditText editTextPhone, editTextMessage;
    Button buttonSendSms;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editTextPhone = findViewById(R.id.editTextPhone);
        editTextMessage = findViewById(R.id.editTextMessage);

        buttonSendSms = findViewById(R.id.buttonSendSms);
        buttonSendSms.setOnClickListener(buttonSendSmsOnClick);
    }

    View.OnClickListener buttonSendSmsOnClick = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            Intent smsIntent = new Intent(Intent.ACTION_VIEW);

            //smsIntent.setType("vnd.android-dir/mms-sms");
            smsIntent.setData(Uri.parse("sms:"));

            smsIntent.putExtra("address", editTextPhone.getText().toString());
            smsIntent.putExtra("sms_body",editTextMessage.getText().toString());

            startActivity(smsIntent);
        }
    };
}
