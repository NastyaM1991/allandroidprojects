package com.example.mytestfragmentapp.commonentities;

public class Character
{
    public String name;
    public String race;
    public int power;
    public int agility;
    public int mana;

    public Character()
    {
        name = "";
        race = "";
        power = agility = mana = 0;
    }

    public String getStringInfo()
    {
        return "Name: " + name + "\n" +
                "Race: " + race + "\n" +
                "Power: " + power + "\n" +
                "Agility: " + agility + "\n" +
                "Mana: " + mana;
    }

}
