package com.example.mytestfragmentapp;

import android.os.Bundle;

import android.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import com.example.mytestfragmentapp.commonentities.DataTransferClass;


public class SkillsFragment extends Fragment
{
    TextView textViewPower, textViewAgility, textViewMana;
    SeekBar seekBarPower, seekBarAgility, seekBarMana;
    Button buttonSkillsApply;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_skills, container, false);

        textViewPower = view.findViewById(R.id.textViewPower);
        textViewAgility = view.findViewById(R.id.textViewAgility);
        textViewMana = view.findViewById(R.id.textViewMana);

        seekBarPower = view.findViewById(R.id.seekBarPower);
        seekBarPower.setOnSeekBarChangeListener(seekBarPowerOnSeek);

        seekBarAgility = view.findViewById(R.id.seekBarAgility);
        seekBarAgility.setOnSeekBarChangeListener(seekBarAgilityOnSeek);

        seekBarMana = view.findViewById(R.id.seekBarMana);
        seekBarMana.setOnSeekBarChangeListener(seekBarManaOnSeek);

        buttonSkillsApply = view.findViewById(R.id.buttonSkillsApply);
        buttonSkillsApply.setOnClickListener(buttonSkillsApplyOnClick);
        return view;
    }

    SeekBar.OnSeekBarChangeListener seekBarPowerOnSeek = new SeekBar.OnSeekBarChangeListener()
    {
        @Override
        public void onProgressChanged(SeekBar seekBar, int i, boolean b)
        {
            textViewPower.setText("Power: " + seekBarPower.getProgress());
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar)
        {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar)
        {

        }
    };

    SeekBar.OnSeekBarChangeListener seekBarAgilityOnSeek = new SeekBar.OnSeekBarChangeListener()
    {
        @Override
        public void onProgressChanged(SeekBar seekBar, int i, boolean b)
        {
            textViewAgility.setText("Agility: " + seekBarAgility.getProgress());
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar)
        {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar)
        {

        }
    };

    SeekBar.OnSeekBarChangeListener seekBarManaOnSeek = new SeekBar.OnSeekBarChangeListener()
    {
        @Override
        public void onProgressChanged(SeekBar seekBar, int i, boolean b)
        {
            textViewMana.setText("Mana: " + seekBarMana.getProgress());
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar)
        {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar)
        {

        }
    };

    View.OnClickListener buttonSkillsApplyOnClick = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            DataTransferClass.character.power = seekBarPower.getProgress();
            DataTransferClass.character.agility = seekBarAgility.getProgress();
            DataTransferClass.character.mana = seekBarMana.getProgress();

            seekBarPower.setProgress(0);
            seekBarAgility.setProgress(0);
            seekBarMana.setProgress(0);

            DataTransferClass.textViewCharacterInfo.setText(DataTransferClass.character.getStringInfo());
        }
    };
}
