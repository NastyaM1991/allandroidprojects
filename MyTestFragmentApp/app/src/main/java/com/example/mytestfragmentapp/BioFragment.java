package com.example.mytestfragmentapp;

import android.os.Bundle;

import android.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.mytestfragmentapp.commonentities.DataTransferClass;

public class BioFragment extends Fragment
{
    EditText editTextName;
    Spinner spinnerRace;
    Button buttonApply;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_bio, container, false);

        editTextName = view.findViewById(R.id.editTextName);

        spinnerRace = view.findViewById(R.id.spinnerRace);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(inflater.getContext(), android.R.layout.simple_spinner_dropdown_item, new String[]{"Man", "Elf", "Dwarf"});
        spinnerRace.setAdapter(adapter);

        buttonApply = view.findViewById(R.id.buttonApply);
        buttonApply.setOnClickListener(buttonApplyOnClick);
        return view;
    }

    View.OnClickListener buttonApplyOnClick = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            DataTransferClass.character.name = editTextName.getText().toString();
            DataTransferClass.character.race = spinnerRace.getSelectedItem().toString();

            DataTransferClass.textViewCharacterInfo.setText(DataTransferClass.character.getStringInfo());
        }
    };
}
