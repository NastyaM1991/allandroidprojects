package com.example.mytestfragmentapp;

import androidx.appcompat.app.AppCompatActivity;

import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.example.mytestfragmentapp.commonentities.DataTransferClass;

public class MainActivity extends AppCompatActivity
{
    Button buttonSetBio, buttonSetSkills;
    TextView textViewCharacterInfo;
    FrameLayout frameLayoutSettings;

    BioFragment bioFragment;
    SkillsFragment skillsFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonSetBio = findViewById(R.id.buttonSetBio);
        buttonSetBio.setOnClickListener(buttonSetBioOnClick);

        buttonSetSkills = findViewById(R.id.buttonSetSkills);
        buttonSetSkills.setOnClickListener(buttonSetSkillsOnClick);

        textViewCharacterInfo = findViewById(R.id.textViewCharacterInfo);
        DataTransferClass.textViewCharacterInfo = textViewCharacterInfo;

        frameLayoutSettings = findViewById(R.id.frameLayoutSettings);

        bioFragment = new BioFragment();
        skillsFragment = new SkillsFragment();
    }

    View.OnClickListener buttonSetBioOnClick = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();

            fragmentTransaction.replace(R.id.frameLayoutSettings, bioFragment);
            fragmentTransaction.commit();
        }
    };

    View.OnClickListener buttonSetSkillsOnClick = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();

            fragmentTransaction.replace(R.id.frameLayoutSettings, skillsFragment);
            fragmentTransaction.commit();
        }
    };
}
