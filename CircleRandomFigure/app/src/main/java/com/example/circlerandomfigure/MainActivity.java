package com.example.circlerandomfigure;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.Random;

public class MainActivity extends AppCompatActivity
{
    static Random random = new Random();

    MyCanvas myCanvas;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        myCanvas = new MyCanvas(getApplicationContext());
        setContentView(myCanvas);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.my_canvas_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.itemReset:
                myCanvas.resetCanvas();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    public class MyCanvas extends View
    {
        private Paint paint;
        private Path path;

        private Paint circlePaint;
        private Path circlePath;

        ArrayList<Point> points;

        int x, y;

        int x0, y0;
        int r;

        double angle;

        public void resetCanvas()
        {
            path.reset();
            circlePath.reset();

            points = new ArrayList<>();

            x0 = 500;
            y0 = 700;

            r = getRandomInRange(200, 400);

            int n = getRandomInRange(3, 15);

            for (int i = 0; i < n; i++)
            {
                angle = i * (360 / n);

                x = (int) (x0 + r * Math.cos(Math.toRadians(angle)));
                y = (int) (y0 + r * Math.sin(Math.toRadians(angle)));

                points.add(new Point(x, y));
            }

            for (int i = 0; i < points.size() - 1; i++)
            {
                path.moveTo(points.get(i).getX(), points.get(i).getY());
                path.lineTo(points.get(i + 1).getX(), points.get(i + 1).getY());
            }

            path.moveTo(points.get(points.size() - 1).getX(), points.get(points.size() - 1).getY());
            path.lineTo(points.get(0).getX(), points.get(0).getY());

            invalidate();
        }

        public MyCanvas(Context context)
        {
            super(context);

            points = new ArrayList<>();

            paint = new Paint();
            path = new Path();

            circlePaint = new Paint();
            circlePath = new Path();

            paint.setStyle(Paint.Style.STROKE);
            paint.setColor(Color.GRAY);
            paint.setStrokeWidth(15);
            paint.setStrokeJoin(Paint.Join.ROUND);
            paint.setStrokeCap(Paint.Cap.ROUND);
            paint.setAntiAlias(true);

            circlePaint.setStyle(Paint.Style.STROKE);
            circlePaint.setColor(Color.GREEN);
            circlePaint.setStrokeWidth(15);
            circlePaint.setStrokeJoin(Paint.Join.ROUND);
            circlePaint.setStrokeCap(Paint.Cap.ROUND);
            circlePaint.setAntiAlias(true);

            resetCanvas();
        }

        @Override
        protected void onDraw(Canvas canvas)
        {
            super.onDraw(canvas);

            canvas.drawPath(path, paint);
            canvas.drawPath(circlePath, circlePaint);
        }

        @Override
        public boolean onTouchEvent(MotionEvent event)
        {
            float x = event.getX();
            float y = event.getY();

            switch (event.getAction())
            {
                case MotionEvent.ACTION_DOWN:
                    circlePath.moveTo(x, y);
                    circlePath.lineTo(x, y);
                    invalidate();
                    break;
                case MotionEvent.ACTION_MOVE:
                    circlePath.lineTo(x, y);
                    invalidate();
                    break;
            }
            return true;
        }
    }

    public int getRandomInRange(int min, int max)
    {
        return random.nextInt((max - min) + 1) + min;
    }
}
