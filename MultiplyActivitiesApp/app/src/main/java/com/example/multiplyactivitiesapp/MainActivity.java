package com.example.multiplyactivitiesapp;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity
{
    EditText editTextInput;
    Button buttonOpenActivity;
    TextView textViewOutput;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editTextInput = findViewById(R.id.editTextInput);
        buttonOpenActivity = findViewById(R.id.buttonOpenActivity);
        textViewOutput = findViewById(R.id.textViewOutput);

        buttonOpenActivity.setOnClickListener(buttonActivityOnClick);
    }

    View.OnClickListener buttonActivityOnClick = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            String text = editTextInput.getText().toString();

            Intent intent = new Intent(getApplicationContext(), HelpActivity.class);
            intent.putExtra("text", text);
            //startActivity(intent);
            startActivityForResult(intent, 1);
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        String text = data.getExtras().getString("text2");
        textViewOutput.setText(text);
    }
}
