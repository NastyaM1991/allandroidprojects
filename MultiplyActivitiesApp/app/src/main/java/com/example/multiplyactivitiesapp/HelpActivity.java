package com.example.multiplyactivitiesapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class HelpActivity extends AppCompatActivity
{
    TextView textViewData;
    Button buttonCloseActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);

        textViewData = findViewById(R.id.textViewData);

        String text = getIntent().getExtras().getString("text") + " JAVA";

        textViewData.setText(text);

        buttonCloseActivity = findViewById(R.id.buttonCloseActivity);
        buttonCloseActivity.setOnClickListener(buttonCloseActivityOnClick);
    }

    View.OnClickListener buttonCloseActivityOnClick = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            Intent data = new Intent();
            data.putExtra("text2", textViewData.getText().toString());
            setResult(RESULT_OK, data);
            finish();
        }
    };
}
