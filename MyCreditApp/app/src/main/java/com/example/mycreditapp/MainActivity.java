package com.example.mycreditapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.Switch;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity
{
    TextView textViewResult;
    RadioButton radioButtonMale, radioButtonFemale;
    Switch switchIsWork;
    CheckBox checkBoxChild, checkBoxIncome;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textViewResult = findViewById(R.id.textViewResult);

        radioButtonMale = findViewById(R.id.radioButtonMale);
        radioButtonFemale = findViewById(R.id.radioButtonFemale);

        switchIsWork = findViewById(R.id.switchIsWork);

        checkBoxChild = findViewById(R.id.checkBoxChild);
        checkBoxIncome = findViewById(R.id.checkBoxIncome);

        calculate();

        radioButtonMale.setOnCheckedChangeListener(onChecked);
        radioButtonFemale.setOnCheckedChangeListener(onChecked);

        switchIsWork.setOnCheckedChangeListener(onChecked);

        checkBoxChild.setOnCheckedChangeListener(onChecked);
        checkBoxIncome.setOnCheckedChangeListener(onChecked);
    }

    CompoundButton.OnCheckedChangeListener onChecked = new CompoundButton.OnCheckedChangeListener()
    {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b)
        {
            calculate();
        }
    };


    void calculate()
    {
        double baseCredit = 1000;

        if (radioButtonMale.isChecked())
        {
            baseCredit *= 1.1;
        }

        if (radioButtonFemale.isChecked())
        {
            baseCredit *= 0.7;
        }

        if (switchIsWork.isChecked())
        {
            baseCredit *= 3.5;
        }
        else
        {
            baseCredit *= 0.5;
        }

        if (checkBoxChild.isChecked())
        {
            baseCredit *= 1.7;
        }
        else
        {
            baseCredit *= 0.9;
        }

        if (checkBoxIncome.isChecked())
        {
            baseCredit *= 2.3;
        }
        else
        {
            baseCredit *= 0.8;
        }

        int baseCreditInt = (int) baseCredit;

        textViewResult.setText("Вам дадут: " + baseCreditInt + " руб");
    }
}
