package com.example.numbersapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity
{
    EditText editTextNumber;
    Button buttonAddToList, buttonClearList;
    ListView listViewNumbers;
    ArrayList<Integer> numbers;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        numbers = new ArrayList<>();

        editTextNumber = findViewById(R.id.editTextNumber);

        buttonAddToList = findViewById(R.id.buttonAddToList);
        buttonClearList = findViewById(R.id.buttonClearList);

        listViewNumbers = findViewById(R.id.listViewNumbers);

        buttonAddToList.setOnClickListener(buttonAddToListOnClick);
        buttonClearList.setOnClickListener(buttonClearListOnClick);
    }

    View.OnClickListener buttonAddToListOnClick = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            int number = Integer.parseInt(editTextNumber.getText().toString());
            numbers.add(number);

            sortNumbers();
            refreshListViewNumbers();
        }
    };

    View.OnClickListener buttonClearListOnClick = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            numbers.clear();
            refreshListViewNumbers();
        }
    };

    void refreshListViewNumbers()
    {
        ArrayAdapter<Integer> adapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_list_item_1, numbers);
        listViewNumbers.setAdapter(adapter);
    }

    void sortNumbers()
    {
        int temp;
        boolean sort;
        int step = 0;

        do
        {
            sort = true;

            for (int i = 0; i < numbers.size() - 1 - step; i++)
            {
                if (numbers.get(i + 1) < numbers.get(i))
                {
                    temp = numbers.get(i);
                    numbers.set(i, numbers.get(i + 1));
                    numbers.set(i + 1, temp);

                    sort = false;
                }
            }
            step++;
        }
        while (!sort);
    }
}
