package com.example.converterapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.text.DecimalFormat;

public class MainActivity extends AppCompatActivity {

    EditText editTextMoney, editTextCourse, editTextCurrency;
    Button buttonConvert;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editTextMoney = findViewById(R.id.editTextMoney);
        editTextCourse = findViewById(R.id.editTextCourse);
        editTextCurrency = findViewById(R.id.editTextCurrency);

        buttonConvert = findViewById(R.id.buttonConvert);
        buttonConvert.setOnClickListener(buttonConvertOnClick);
    }

    View.OnClickListener buttonConvertOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            double money = Double.parseDouble(editTextMoney.getText().toString());
            double course = Double.parseDouble(editTextCourse.getText().toString());

            double currency = money / course;
            DecimalFormat format = new DecimalFormat("#.00");
            editTextCurrency.setText(format.format(currency));
        }
    };
}
