package com.example.bodymassindex;

public class ImtNorm
{
    int ageFrom;
    int ageTo;
    double imt;

    public ImtNorm(int ageFrom, int ageTo, double imt)
    {
        this.ageFrom = ageFrom;
        this.ageTo = ageTo;
        this.imt = imt;
    }

    public int getAgeFrom()
    {
        return ageFrom;
    }

    public int getAgeTo()
    {
        return ageTo;
    }

    public double getImt()
    {
        return imt;
    }
}
