package com.example.bodymassindex;

import java.util.ArrayList;

public class ImtReference
{
    private ArrayList<ImtNorm> maleNorms;
    private ArrayList<ImtNorm> femaleNorms;

    public ImtReference()
    {
        maleNorms = new ArrayList<>();
        femaleNorms = new ArrayList<>();
        imtLoader();
    }

    public double getImt(Gender gender, int age)
    {
        if (gender == Gender.MALE)
        {
            return getImtByGender(maleNorms, age);
        }
        else
        {
            return getImtByGender(femaleNorms, age);
        }
    }

    private double getImtByGender(ArrayList<ImtNorm> norms, int age)
    {
        double imt;

        for (ImtNorm norm : norms)
        {
            if (age >= norm.getAgeFrom() && age <= norm.getAgeTo())
            {
                return norm.getImt();
            }
        }

        return 0.0;
    }

    private void imtLoader()
    {
        loadMales(); //имитация загрузки из файла males.txt
        loadFemales(); //имитация загрузки из файла females.txt
    }

    private void loadMales()
    {
        //мальчики
        maleNorms.add(new ImtNorm(1, 1, 17.2));
        maleNorms.add(new ImtNorm(2, 2, 16.5));
        maleNorms.add(new ImtNorm(3, 3, 16.0));
        maleNorms.add(new ImtNorm(4, 4, 15.8));
        maleNorms.add(new ImtNorm(5, 5, 15.5));
        maleNorms.add(new ImtNorm(6, 6, 15.4));
        maleNorms.add(new ImtNorm(7, 7, 15.5));
        maleNorms.add(new ImtNorm(8, 8, 16.4));
        maleNorms.add(new ImtNorm(9, 9, 17.1));
        maleNorms.add(new ImtNorm(10, 10, 17.1));
        maleNorms.add(new ImtNorm(11, 11, 17.8));
        maleNorms.add(new ImtNorm(12, 12, 18.4));
        maleNorms.add(new ImtNorm(13, 13, 19.1));
        maleNorms.add(new ImtNorm(14, 14, 19.8));
        maleNorms.add(new ImtNorm(15, 15, 20.2));
        maleNorms.add(new ImtNorm(16, 16, 21.0));
        maleNorms.add(new ImtNorm(17, 17, 21.6));
        //мужчины
        maleNorms.add(new ImtNorm(18, 24, 21.4));
        maleNorms.add(new ImtNorm(25, 34, 21.6));
        maleNorms.add(new ImtNorm(35, 44, 22.9));
        maleNorms.add(new ImtNorm(45, 54, 25.8));
        maleNorms.add(new ImtNorm(55, 120, 26.6));

    }

    private void loadFemales()
    {
        //девочки
        femaleNorms.add(new ImtNorm(1, 1, 16.6));
        femaleNorms.add(new ImtNorm(2, 2, 16.0));
        femaleNorms.add(new ImtNorm(3, 3, 15.6));
        femaleNorms.add(new ImtNorm(4, 4, 15.4));
        femaleNorms.add(new ImtNorm(5, 5, 15.3));
        femaleNorms.add(new ImtNorm(6, 6, 15.3));
        femaleNorms.add(new ImtNorm(7, 7, 15.5));
        femaleNorms.add(new ImtNorm(8, 8, 15.9));
        femaleNorms.add(new ImtNorm(9, 9, 16.4));
        femaleNorms.add(new ImtNorm(10, 10, 16.9));
        femaleNorms.add(new ImtNorm(11, 11, 17.7));
        femaleNorms.add(new ImtNorm(12, 12, 18.4));
        femaleNorms.add(new ImtNorm(13, 13, 18.9));
        femaleNorms.add(new ImtNorm(14, 14, 19.4));
        femaleNorms.add(new ImtNorm(15, 15, 20.2));
        femaleNorms.add(new ImtNorm(16, 16, 20.3));
        femaleNorms.add(new ImtNorm(17, 17, 20.5));
        //женщины
        femaleNorms.add(new ImtNorm(18, 24, 19.5));
        femaleNorms.add(new ImtNorm(25, 34, 23.2));
        femaleNorms.add(new ImtNorm(35, 44, 23.4));
        femaleNorms.add(new ImtNorm(45, 54, 25.7));
        femaleNorms.add(new ImtNorm(55, 120, 27.3));
    }
}
