package com.example.bodymassindex;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.text.DecimalFormat;

public class MainActivity extends AppCompatActivity
{
    private static final double EPS = 0.25; //погрешность сравнения ИМТ
    EditText editTextWeight, editTextHeight, editTextAge, editTextBodyMassIndex;
    Spinner spinnerGender;
    Button buttonCalculate;
    TextView textViewResult;

    String[] gender = new String[]{"мужчина", "женщина"};

    ImtReference imtReference;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editTextWeight = findViewById(R.id.editTextWeight);
        editTextHeight = findViewById(R.id.editTextHeight);
        editTextAge = findViewById(R.id.editTextAge);
        editTextBodyMassIndex = findViewById(R.id.editTextBodyMassIndex);

        textViewResult = findViewById(R.id.textViewResult);

        spinnerGender = findViewById(R.id.spinnerGender);
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, gender);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerGender.setAdapter(spinnerAdapter);

        buttonCalculate = findViewById(R.id.buttonCalculate);
        buttonCalculate.setOnClickListener(buttonCalculateOnClick);

        imtReference = new ImtReference();
    }

    View.OnClickListener buttonCalculateOnClick = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            double weight = Double.parseDouble(editTextWeight.getText().toString());
            double height = Double.parseDouble(editTextHeight.getText().toString()) / 100;

            double imt = weight / (height * height);
            DecimalFormat format = new DecimalFormat("#.00");
            editTextBodyMassIndex.setText(format.format(imt));

            int genderPosition = spinnerGender.getSelectedItemPosition();
            Gender gender = Gender.values()[genderPosition];
            int age = Integer.parseInt(editTextAge.getText().toString());

            textViewResult.setText(getResult(gender, age, imt));
        }
    };

    private String getResult(Gender gender, int age, double imt)
    {
        double normalImt = imtReference.getImt(gender, age);
        if (Math.abs(imt-normalImt) <= EPS)
        {
            return "Нормальная масса тела";
        }
        else if (imt < (normalImt - EPS))
        {
            return "Недостаточная масса тела";
        }
        else
        {
            return "Избыточная масса тела";
        }
    }
}
