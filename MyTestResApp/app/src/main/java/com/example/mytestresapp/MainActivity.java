package com.example.mytestresapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity
{
    TextView textViewTestCode;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textViewTestCode = findViewById(R.id.textViewTestCode);
        String text = getResources().getString(R.string.code_key);
        textViewTestCode.setText(text);
    }
}
