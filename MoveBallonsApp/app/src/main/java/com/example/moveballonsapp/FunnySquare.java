package com.example.moveballonsapp;

import android.graphics.Canvas;
import android.graphics.Paint;

import java.util.Random;

public class FunnySquare
{
    static Random random = new Random();

    private int getRandomInRange(int min, int max)
    {
        return random.nextInt((max - min) + 1) + min;
    }

    private int left, width, top, height;
    private int dx, dy;
    private Paint paint;

    public FunnySquare()
    {
        left = 0;
        top = 0;

        width = getRandomInRange(50, 200);
        height = getRandomInRange(50, 200);

        dx = getRandomInRange(5, 10);
        dy = getRandomInRange(5, 10);

        paint = new Paint();
        paint.setARGB(255, getRandomInRange(0, 255), getRandomInRange(0, 255), getRandomInRange(0, 255));
    }

    public void move(int xMin, int xMax, int yMin, int yMax)
    {
        left += dx;
        top += dy;

        if (left + width >= xMax || left <= xMin)
        {
            dx = -dx;
        }
        if (top + height >= yMax || top <= yMin)
        {
            dy = -dy;
        }
    }

    public void draw(Canvas canvas)
    {
        canvas.drawRect(left, top, left + width, top + height, paint);
    }

    public boolean isPointInArea(int x, int y)
    {
        return x >= left && x <= left + width && y >= top && y <= top + height;
    }
}
