package com.example.moveballonsapp;

import android.graphics.Canvas;

import java.util.ArrayList;

public class FunnySquaresList
{
    private ArrayList<FunnySquare> funnySquares;

    public FunnySquaresList(int size)
    {
        funnySquares = new ArrayList<>();

        for (int i = 0; i < size; i++)
        {
            funnySquares.add(new FunnySquare());
        }
    }

    public void drawAll(Canvas canvas)
    {
        for (int i = 0; i < funnySquares.size(); i++)
        {
            funnySquares.get(i).draw(canvas);
        }
    }

    public void moveAll(int xMin, int xMax, int yMin, int yMax)
    {
        for (int i = 0; i < funnySquares.size(); i++)
        {
            funnySquares.get(i).move(xMin, xMax, yMin, yMax);
        }
    }

    public boolean deleteTouched(int x, int y)
    {
        for (int i = 0; i < funnySquares.size(); i++)
        {
            if (funnySquares.get(i).isPointInArea(x, y))
            {
                funnySquares.remove(i);
                return true;
            }
        }
        return false;
    }

    public void generateNewIfSmallerThanBorder(int border, int size)
    {
        if (funnySquares.size() < border)
        {
            for (int i = 0; i < size; i++)
            {
                funnySquares.add(new FunnySquare());
            }
        }
    }
}
