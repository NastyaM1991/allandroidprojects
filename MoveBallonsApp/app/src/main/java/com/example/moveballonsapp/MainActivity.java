package com.example.moveballonsapp;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        /*View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);*/

        setContentView(new MyCanvas(getApplicationContext()));
    }

    public class MyCanvas extends View
    {
        int xMin, yMin;
        int xMax, yMax;
        Paint p;
        
        FunnySquaresList funnySquaresList;

        int countTouched;

        public MyCanvas(Context context)
        {
            super(context);

            xMin = 0;
            yMin = 0;

            funnySquaresList = new FunnySquaresList(50);
            countTouched = 0;

            p = new Paint();
            p.setTextSize(50);
        }

        @Override
        protected void onDraw(Canvas canvas)
        {
            super.onDraw(canvas);

            canvas.drawColor(Color.WHITE);

            canvas.drawText("Deleted: " + countTouched, xMax / 2 - 100, yMax / 2, p);

            funnySquaresList.drawAll(canvas);

            funnySquaresList.moveAll(xMin, xMax, yMin, yMax);

            invalidate();
        }

        @Override
        public boolean onTouchEvent(MotionEvent event)
        {
            int x = (int) event.getX();
            int y = (int) event.getY();

            switch (event.getAction())
            {
                case MotionEvent.ACTION_DOWN:
                    if (funnySquaresList.deleteTouched(x, y))
                    {
                        countTouched++;
                    }

                    funnySquaresList.generateNewIfSmallerThanBorder(1, 50);

                    invalidate();
                    break;
            }

            return super.onTouchEvent(event);
        }

        @Override
        protected void onSizeChanged(int w, int h, int oldw, int oldh)
        {
            super.onSizeChanged(w, h, oldw, oldh);

            xMax = w;
            yMax = h;
        }
    }
}
