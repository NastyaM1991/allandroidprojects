package com.example.listprojectapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity
{
    Button buttonEnter;
    TextView textViewMainName, textViewDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textViewMainName = findViewById(R.id.textViewMainName);
        textViewMainName.setText("МОНУМЕНТ");

        textViewDescription = findViewById(R.id.textViewDescription);
        textViewDescription.setText("Памятники и монументы Брянщины, посвящённые героям и событиям Великой Отечественной войны 1941-1945 годов");

        buttonEnter = findViewById(R.id.buttonEnter);
        buttonEnter.setOnClickListener(buttonEnterOnClick);
    }

    View.OnClickListener buttonEnterOnClick = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            Intent intent = new Intent(getApplicationContext(), ListViewActivity.class);
            startActivity(intent);
        }
    };

}
