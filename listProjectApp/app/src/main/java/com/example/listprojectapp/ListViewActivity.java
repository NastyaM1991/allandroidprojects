package com.example.listprojectapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class ListViewActivity extends AppCompatActivity
{
    ListView listViewMonuments;

    ArrayList<String> monuments;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view);


        listViewMonuments = findViewById(R.id.listViewMonuments);

        monuments = new ArrayList<>();
        monuments.add("Памятный знак воинам-танкистам");
        monuments.add("Мемориальный комплекс «Курган Бессмертия»");
        monuments.add("Символический памятник учителям");
        monuments.add("Памятник воинской славы");

        ArrayAdapter<String> adapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_list_item_1, monuments);
        listViewMonuments.setAdapter(adapter);

        listViewMonuments.setOnItemClickListener(listViewOnItemClick);
    }

    ListView.OnItemClickListener listViewOnItemClick = new AdapterView.OnItemClickListener()
    {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
        {
            switch (i)
            {
                case 0:
                    Intent intent1 = new Intent(getApplicationContext(), ListItem0.class);
                    startActivity(intent1);
                    break;
                case 1:
                    Intent intent2 = new Intent(getApplicationContext(), ListItem1.class);
                    startActivity(intent2);
                    break;
                case 2:
                    Intent intent3 = new Intent(getApplicationContext(), ListItem2.class);
                    startActivity(intent3);
                    break;
                case 3:
                    Intent intent4 = new Intent(getApplicationContext(), ListItem3.class);
                    startActivity(intent4);
                    break;
            }
        }
    };
}
