package com.example.listprojectapp;

import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.TextView;
import android.widget.ViewFlipper;

import androidx.appcompat.app.AppCompatActivity;

public class ListItem1 extends AppCompatActivity
{
    TextView textViewItem1, textViewItem1Name, textViewItem1GPS;
    ViewFlipper viewFlipper1;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_item1);

        textViewItem1Name = findViewById(R.id.textViewItem1Name);
        textViewItem1Name.setText("Мемориальный комплекс «Курган Бессмертия» советским воинам и партизанам, погибшим в боях с немецко-фашистскими захватчиками в годы Великой Отечественной войны");

        textViewItem1GPS = findViewById(R.id.textViewItem1GPS);
        textViewItem1GPS.setText("GPS координаты: 53,268683 34,364428");

        viewFlipper1 = findViewById(R.id.viewFlipper1);
        viewFlipper1.setOnTouchListener(new OnFlingListener(this)
        {

            @Override
            public void onRightToLeft()
            {

                viewFlipper1.setInAnimation(getApplicationContext(), R.anim.right_to_left);

                viewFlipper1.showPrevious();
            }

            @Override
            public void onLeftToRight()
            {

                viewFlipper1.setInAnimation(getApplicationContext(), R.anim.left_to_right);
                viewFlipper1.showNext();
            }

            @Override
            public void onBottomToTop()
            {

            }

            @Override
            public void onTopToBottom()
            {

            }
        });

        viewFlipper1.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {

            }
        });

        textViewItem1 = findViewById(R.id.textViewItem1);
        textViewItem1.setMovementMethod(new ScrollingMovementMethod());
        textViewItem1.setText("Местонахождение: Брянская область, г. Брянск, Центральный парк культуры и отдыха имени 1000-летия г. Брянска\n" +
                "\n" +
                "Автор проекта архитектор В. Н. Городков, автор пилона В. А. Тепляков, автор мозаичных панно В. П. Кочетков\n" +
                "\n" +
                "Курган Бессмертия был заложен 7 мая 1967 года, сооружён в 1968 году. Памятник на Кургане Бессмертия был торжественно открыт 17 сентября 1976 года.\n" +
                "\n" +
                "Описание\n" +
                "\n" +
                "Представляет собой искусственную земляную насыпь в виде пятигранного холма общим объёмом свыше 20 тысяч кубических метров и высотой 12 метров, которую венчает 18-метровый бетонный пилон в виде пятиконечной звезды, состоящей из отдельных элементов-«лепестков», связанных мозаичным кольцом.\n" +
                "\n" +
                "История создания\n" +
                "\n" +
                "Курган Бессмертия был заложен 7 мая 1967 года. К месту его закладки была доставлена земля с братских могил из городов и сёл Брянщины, из\n" +
                "\n" +
                "городов-героев СССР, с болгарской Шипки. В торжественной церемонии закладки Кургана приняли участие матери солдат, погибших в годы Великой Отечественной войны, ветераны войны, революции и комсомола, Герои Советского Союза, кавалеры орденов Славы, руководители и участники партизанского движения и подполья, воины Советской Армии и молодёжь города.\n" +
                "\n" +
                "Сооружение Кургана было приурочено к 50-летию Октября. В основании Кургана зарыт орудийный ствол с капсулой, в которой содержится обращение к потомкам-брянцам 2017 года.\n" +
                "\n" +
                "Основные работы по формированию Кургана проводились в 1967-1968 гг. силами брянских комсомольцев. У основания Кургана ступени широкой лестницы подводят к площадке, на фасаде которой надпись: «Жителям Брянска, павшим в боях за свободу и независимость нашей Родины. КУРГАН БЕССМЕРТИЯ сооружён в 1968 году». Далее две небольшие гранитные лестницы ведут на верхнюю площадку кургана, откуда открывается широкий обзор на левобережье Десны.\n" +
                "\n" +
                "28 октября 1968 г., в канун 50-летнего юбилея ВЛКСМ, была открыта первая очередь Кургана. К этому времени на вершине было высажено кольцо молодых берёз, в центре которого впоследствии планировалось создание панорамного музея боевой славы, увенчанного обелиском с красной звездой.\n" +
                "\n" +
                "К началу 1970-х гг. первоначальный проект Кургана Бессмертия был пересмотрен и учреждён конкурс работ на завершение Кургана. Его итоги подвели в апреле 1975 г. Из 14 представленных работ жюри выбрало проект группы архитекторов во главе с В. А. Тепляковым, который и был реализован.\n" +
                "\n" +
                "В 1976 году была завершена вторая очередь Кургана. На верхней площадке был установлен пилон в виде пятиконечной звезды, состоящей из отдельных элементов-«лепестков», связанных мозаичным кольцом.\n" +
                "\n" +
                "Памятник на Кургане Бессмертия был торжественно открыт 17 сентября 1976 года.");
    }
}
