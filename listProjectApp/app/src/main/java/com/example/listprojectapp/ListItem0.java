package com.example.listprojectapp;

import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.TextView;
import android.widget.ViewFlipper;

import androidx.appcompat.app.AppCompatActivity;

public class ListItem0 extends AppCompatActivity
{
    TextView textViewItem0, textViewItem0Name, textViewItem0GPS;
    ViewFlipper viewFlipper;

    @Override

    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_item0);

        textViewItem0Name = findViewById(R.id.textViewItem0Name);
        textViewItem0Name.setText("Памятный знак (танк Т-34) воинам-танкистам 11-й армии Брянского фронта, погибшим в боях за Родину в годы Великой Отечественной войны");

        textViewItem0GPS = findViewById(R.id.textViewItem0GPS);
        textViewItem0GPS.setText("GPS координаты: 53,222411 34,367514");

        viewFlipper = findViewById(R.id.viewFlipper);
        viewFlipper.setOnTouchListener(new OnFlingListener(this)
        {

            @Override
            public void onRightToLeft()
            {

                viewFlipper.setInAnimation(getApplicationContext(), R.anim.right_to_left);

                viewFlipper.showPrevious();
            }

            @Override
            public void onLeftToRight()
            {

                viewFlipper.setInAnimation(getApplicationContext(), R.anim.left_to_right);
                viewFlipper.showNext();
            }

            @Override
            public void onBottomToTop()
            {

            }

            @Override
            public void onTopToBottom()
            {

            }
        });

        viewFlipper.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {

            }
        });

        textViewItem0 = findViewById(R.id.textViewItem0);
        textViewItem0.setMovementMethod(new ScrollingMovementMethod());
        textViewItem0.setText("Местонахождение: Брянская область, г. Брянск, дамба у р. Десны\n" +
                "\n" +
                "Установлен в 1967 г. Архитектор К.И. Митин.\n" +
                "\n" +
                "Описание\n" +
                "\n" +
                "Представляет собой четырехугольный железобетонный пьедестал. Его высота четыре метра. На нем установлен танк Т-34/85 с бортовым знаком 242-знаком первого танка, ворвавшегося в город. Памятник сооружен по инициативе и на средства предприятий и общественных организаций Фокинского района города Брянска. Его открытие состоялось в день двадцать пятой годовщины освобождения Брянска от немецко-фашистских захватчиков.");
    }

}
