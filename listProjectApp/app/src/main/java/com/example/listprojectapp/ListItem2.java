package com.example.listprojectapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.TextView;
import android.widget.ViewFlipper;

public class ListItem2 extends AppCompatActivity
{
    TextView textViewItem2, textViewItem2Name, textViewItem2GPS;
    ViewFlipper viewFlipper2;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_item2);

        textViewItem2Name = findViewById(R.id.textViewItem2Name);
        textViewItem2Name.setText("Символический памятник учителям, отдавшим жизнь в войне с фашизмом");

        textViewItem2GPS = findViewById(R.id.textViewItem2GPS);
        textViewItem2GPS.setText("GPS координаты: 53,271986 34,352121");

        viewFlipper2 = findViewById(R.id.viewFlipper2);
        viewFlipper2.setOnTouchListener(new OnFlingListener(this)
        {

            @Override
            public void onRightToLeft()
            {

                viewFlipper2.setInAnimation(getApplicationContext(), R.anim.right_to_left);

                viewFlipper2.showPrevious();
            }

            @Override
            public void onLeftToRight()
            {

                viewFlipper2.setInAnimation(getApplicationContext(), R.anim.left_to_right);
                viewFlipper2.showNext();
            }

            @Override
            public void onBottomToTop()
            {

            }

            @Override
            public void onTopToBottom()
            {

            }
        });

        viewFlipper2.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {

            }
        });

        textViewItem2 = findViewById(R.id.textViewItem2);
        textViewItem2.setMovementMethod(new ScrollingMovementMethod());
        textViewItem2.setText("Местонахождение: Брянская область, г. Брянск, ул. Бежицкая, д.14\n" +
                "\n" +
                "Установлен в 1981 г. Автор трехметровой бронзовой скульптуры, отлитой ленинградскими мастерами, скульптор Алексей Кобелинец.\n" +
                "\n" +
                "Описание\n" +
                "\n" +
                "Представляет собой фигуру учителя, которого клонит к земле. Однако в ней еще есть силы. Чувствуется твердость руки, которая прикрывает простреленную книгу. Это обобщенный образ учителя, ценой собственной жизни защитившего мир от фашизма. Бронзовая фигура как бы напоминает нынешнему поколению и будет напоминать потомкам о том вкладе, который внесли учителя в Великую Победу, о долге педагога.");
    }
}
