package com.example.listprojectapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.TextView;
import android.widget.ViewFlipper;

public class ListItem3 extends AppCompatActivity
{
    TextView textViewItem3, textViewItem3Name, textViewItem3GPS;
    ViewFlipper viewFlipper3;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_item3);

        textViewItem3Name = findViewById(R.id.textViewItem3Name);
        textViewItem3Name.setText("Памятник воинской славы (85-мм зенитное орудие) воинам 11-й и 11-й гвардейской армий Брянского фронта, освободившим 17 сентября 1943 года города Брянск и Бежицу от немецко-фашистских захватчиков.");

        textViewItem3GPS = findViewById(R.id.textViewItem3GPS);
        textViewItem3GPS.setText("GPS координаты: 53,293706 34,305425");

        viewFlipper3 = findViewById(R.id.viewFlipper3);
        viewFlipper3.setOnTouchListener(new OnFlingListener(this)
        {

            @Override
            public void onRightToLeft()
            {

                viewFlipper3.setInAnimation(getApplicationContext(), R.anim.right_to_left);

                viewFlipper3.showPrevious();
            }

            @Override
            public void onLeftToRight()
            {

                viewFlipper3.setInAnimation(getApplicationContext(), R.anim.left_to_right);
                viewFlipper3.showNext();
            }

            @Override
            public void onBottomToTop()
            {

            }

            @Override
            public void onTopToBottom()
            {

            }
        });

        viewFlipper3.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {

            }
        });

        textViewItem3 = findViewById(R.id.textViewItem3);
        textViewItem3.setMovementMethod(new ScrollingMovementMethod());
        textViewItem3.setText("Местонахождение: Брянская область, г. Брянск, р. Десна, Первомайский мост\n" +
                "\n" +
                "Установлен в 1967 г. Архитектор В.Н. Городков.\n" +
                "\n" +
                "Описание\n" +
                "\n" +
                "Представляет собой четырехугольный железобетонный пьедестал, высотой в два с половиной метра, на котором установлено 85-миллиметровое зенитное орудие.");
    }
}
