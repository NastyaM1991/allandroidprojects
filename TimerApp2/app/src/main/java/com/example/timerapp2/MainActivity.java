package com.example.timerapp2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity
{
    TextView textViewTime, textViewEnd;
    EditText editTextSec, editTextMin;
    Button buttonStart;

    Timer timer;
    TimerTask timerTask;

    int currentSeconds;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textViewTime = findViewById(R.id.textViewTime);
        textViewEnd = findViewById(R.id.textViewEnd);

        editTextSec = findViewById(R.id.editTextSec);
        editTextMin = findViewById(R.id.editTextMin);
        buttonStart = findViewById(R.id.buttonStart);

        currentSeconds = 0;
        timer = null;
        timerTask = null;

        buttonStart.setOnClickListener(buttonStartOnClick);
    }

    View.OnClickListener buttonStartOnClick = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            buttonStart.setEnabled(false);
            textViewEnd.setText("");
            int secValue = Integer.parseInt(editTextSec.getText().toString());
            int minValue = Integer.parseInt(editTextMin.getText().toString());
            currentSeconds = minValue * 60 + secValue;

            timer = new Timer();

            timerTask = new TimerTask()
            {
                @Override
                public void run()
                {
                    int min;
                    int sec;
                    if (currentSeconds <= 0)
                    {
                        timer.cancel();
                        timer = null;
                        runOnUiThread(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                textViewEnd.setText("Ring-ring!");
                                buttonStart.setEnabled(true);
                            }
                        });
                    }
                    min = currentSeconds / 60;
                    sec = currentSeconds % 60;

                    if (sec == 0 && min > 0)
                    {
                        min -= 1;
                        sec = 59;
                    }

                    final String output = String.format("%02d:%02d", min, sec);

                    runOnUiThread(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            textViewTime.setText(output);
                        }
                    });

                    currentSeconds = min * 60 + sec;
                    currentSeconds--;
                }
            };

            timer.schedule(timerTask, 1000, 1000);
        }
    };
}
