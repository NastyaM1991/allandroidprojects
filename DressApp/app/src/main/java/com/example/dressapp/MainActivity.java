package com.example.dressapp;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity
{
    RadioButton radioButtonKnight, radioButtonWizard;
    Spinner spinnerHead, spinnerBody, spinnerLegs;
    CheckBox checkBoxBoots;
    Button buttonCheck;
    TextView textViewResult;

    String[] head = new String[]{"Шлем", "Шляпа"};
    String[] body = new String[]{"Кольчуга", "Мантия"};
    String[] legs = new String[]{"Доспехи", "Штаны"};


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        radioButtonKnight = findViewById(R.id.radioButtonKnight);
        radioButtonWizard = findViewById(R.id.radioButtonWizard);

        spinnerHead = findViewById(R.id.spinnerHead);
        ArrayAdapter<String> spinnerAdapterHead = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, head);
        spinnerAdapterHead.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerHead.setAdapter(spinnerAdapterHead);

        spinnerBody = findViewById(R.id.spinnerBody);
        ArrayAdapter<String> spinnerAdapterBody = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, body);
        spinnerAdapterBody.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerBody.setAdapter(spinnerAdapterBody);

        spinnerLegs = findViewById(R.id.spinnerLegs);
        ArrayAdapter<String> spinnerAdapterLegs = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, legs);
        spinnerAdapterLegs.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerLegs.setAdapter(spinnerAdapterLegs);

        checkBoxBoots = findViewById(R.id.checkBoxBoots);

        buttonCheck = findViewById(R.id.buttonCheck);

        textViewResult = findViewById(R.id.textViewResult);

        buttonCheck.setOnClickListener(buttonCheckOnClick);
    }

    View.OnClickListener buttonCheckOnClick = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            int headPosition = spinnerHead.getSelectedItemPosition();
            int bodyPosition = spinnerBody.getSelectedItemPosition();
            int legsPosition = spinnerLegs.getSelectedItemPosition();

            if (radioButtonKnight.isChecked() && headPosition == 0 && bodyPosition == 0 && legsPosition == 0 && checkBoxBoots.isChecked())
            {
                textViewResult.setText("Одет верно");
            }
            else if (radioButtonWizard.isChecked() && headPosition == 1 && bodyPosition == 1 && legsPosition == 1 && checkBoxBoots.isChecked())
            {
                textViewResult.setText("Одет верно");
            }
            else
            {
                textViewResult.setText("Одет неверно");
            }
        }
    };
}
