package com.example.myfitnessclientsapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.SeekBar;

import com.example.myfitnessclientsapp.commondata.Contact;
import com.example.myfitnessclientsapp.commondata.DataTransferClass;

import java.util.Date;

public class ContactAddActivity extends AppCompatActivity
{
    EditText editTextName;
    CalendarView calendarViewBirthday;
    SeekBar seekBarWeight;
    Button buttonAddAndClose;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_add);

        editTextName = findViewById(R.id.editTextName);

        calendarViewBirthday = findViewById(R.id.calendarViewBirthday);

        seekBarWeight = findViewById(R.id.seekBarWeight);

        buttonAddAndClose = findViewById(R.id.buttonAddAndClose);
        buttonAddAndClose.setOnClickListener(buttonAddAndCloseOnClick);
    }

    View.OnClickListener buttonAddAndCloseOnClick = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            String name = editTextName.getText().toString();
            Date birthday = new Date(calendarViewBirthday.getDate());
            int weight = seekBarWeight.getProgress();

            DataTransferClass.contactsList.add(new Contact(name, birthday, weight));

            finish();
        }
    };
}
