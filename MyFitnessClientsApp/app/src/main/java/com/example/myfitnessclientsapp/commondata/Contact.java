package com.example.myfitnessclientsapp.commondata;

import java.util.Date;

public class Contact
{
    public String name;
    public Date birthday;
    public int weight;

    public Contact(String name, Date birthday, int weight)
    {
        this.name = name;
        this.birthday = birthday;
        this.weight = weight;
    }
}
