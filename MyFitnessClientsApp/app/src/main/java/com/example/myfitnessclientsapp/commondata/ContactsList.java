package com.example.myfitnessclientsapp.commondata;

import java.util.ArrayList;

public class ContactsList
{
    private ArrayList<Contact> contacts;

    public ContactsList()
    {
        contacts = new ArrayList<>();
    }

    public void add(Contact contact)
    {
        contacts.add(contact);
    }

    public ArrayList<String> getContactsInStrings()
    {
        ArrayList<String> strings = new ArrayList<>();

        for (int i = 0; i < contacts.size(); i++)
        {
            String output = "Name: " + contacts.get(i).name + "\n" + "Birthday: " + contacts.get(i).birthday + "\n" + "Weight: " + contacts.get(i).weight + "\n";

            strings.add(output);
        }

        return strings;
    }
}
