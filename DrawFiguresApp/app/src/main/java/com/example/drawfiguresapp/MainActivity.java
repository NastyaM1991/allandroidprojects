package com.example.drawfiguresapp;

import androidx.appcompat.app.AppCompatActivity;

import android.bluetooth.BluetoothA2dp;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(new DrawView(getApplicationContext()));
    }

    public class DrawView extends View
    {
        Paint paint;
        Rect rect;
        /*RectF rectF;
        float[] points;
        float[] points1;*/
        StringBuilder stringBuilder;

        public DrawView(Context context)
        {
            super(context);

            paint = new Paint();
            //rect = new Rect();
            /*rectF = new RectF(700, 100, 800, 150);
            points = new float[]{100, 50, 150, 100, 150, 200, 50, 200, 50, 100};
            points1 = new float[]{300, 200, 600, 200, 300, 300, 600, 300, 400, 100, 400, 400, 500, 100, 500, 400};*/
            rect = new Rect(100, 200, 200, 300);
            stringBuilder = new StringBuilder();
        }

        @Override
        protected void onDraw(Canvas canvas)
        {
            super.onDraw(canvas);

            //canvas.drawARGB(80, 102, 204, 255);
            canvas.drawColor(Color.WHITE);

            paint.setColor(Color.GREEN);
            paint.setStrokeWidth(10);

            /*canvas.drawPoint(50, 50, paint);

            canvas.drawLine(100, 100, 400, 50, paint);

            canvas.drawCircle(100, 200, 45, paint);

            canvas.drawRect(200, 150, 400, 200, paint);

            rect.set(240, 300, 350, 500);
            canvas.drawRect(rect, paint);*/

            /*canvas.drawPoints(points, paint);

            canvas.drawLines(points1, paint);

            paint.setColor(Color.RED);

            canvas.drawRoundRect(rectF, 20, 20, paint);

            paint.setColor(Color.BLACK);

            rectF.offset(0, 150);
            canvas.drawOval(rectF, paint);

            // смещаем rectF в (900,100)
            rectF.offsetTo(900, 100);
            // увеличиваем rectF по вертикали на 25 вниз и вверх
            rectF.inset(0, -25);

            paint.setColor(Color.YELLOW);
            canvas.drawArc(rectF, 90, 270, true, paint);

            rectF.offset(0, 150);

            paint.setColor(Color.MAGENTA);
            canvas.drawArc(rectF, 90, 270, false, paint);

            paint.setStrokeWidth(3);
            canvas.drawLine(150, 450, 150, 600, paint);

            paint.setColor(Color.GRAY);
            paint.setTextSize(30);

            canvas.drawText("textLeft", 150, 500, paint);

            paint.setTextAlign(Paint.Align.CENTER);
            canvas.drawText("textCenter", 150, 525, paint);

            paint.setTextAlign(Paint.Align.RIGHT);
            canvas.drawText("textRight", 150, 550, paint);*/

            paint.setTextSize(30);

            stringBuilder.setLength(0);
            stringBuilder.append("width = ").append(canvas.getWidth()).append(", height = ").append(canvas.getHeight());
            canvas.drawText(stringBuilder.toString(), 100, 100, paint);

            paint.setStyle(Paint.Style.FILL);
            canvas.drawRect(rect, paint);

            paint.setStyle(Paint.Style.STROKE);
            rect.offset(150, 0);
            canvas.drawRect(rect, paint);

            paint.setStyle(Paint.Style.FILL_AND_STROKE);
            rect.offset(150, 0);
            canvas.drawRect(rect, paint);
        }
    }

}
