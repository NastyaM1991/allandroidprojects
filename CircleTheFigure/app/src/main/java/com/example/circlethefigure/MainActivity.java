package com.example.circlethefigure;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import java.util.Random;

public class MainActivity extends AppCompatActivity
{
    static Random random = new Random();

    MyCanvas myCanvas;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        myCanvas = new MyCanvas(getApplicationContext());
        setContentView(myCanvas);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.my_canvas_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.itemReset:
                myCanvas.resetCanvas();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public class MyCanvas extends View
    {
        private Paint paint;
        private Paint circlePaint;
        private Path path;

        RectF rect;

        int x1, x2, y1, y2;

        public void resetCanvas()
        {
            path.reset();

            x1 = getRandomInRange(200, 800);
            x2 = getRandomInRange(200, 800);
            y1 = getRandomInRange(200, 800);
            y2 = getRandomInRange(200, 800);

            rect = new RectF(x1, y1, x2, y2);

            invalidate();
        }

        public MyCanvas(Context context)
        {
            super(context);

            paint = new Paint();
            circlePaint = new Paint();
            path = new Path();

            paint.setStyle(Paint.Style.STROKE);
            paint.setColor(Color.BLACK);
            paint.setStrokeWidth(15);

            circlePaint.setStyle(Paint.Style.STROKE);
            circlePaint.setColor(Color.GREEN);
            circlePaint.setStrokeWidth(15);
            circlePaint.setStrokeJoin(Paint.Join.ROUND);
            circlePaint.setStrokeCap(Paint.Cap.ROUND);
            circlePaint.setAntiAlias(true);

            resetCanvas();
        }

        @Override
        protected void onDraw(Canvas canvas)
        {
            super.onDraw(canvas);

            canvas.drawRect(rect, paint);
            canvas.drawPath(path, circlePaint);
        }

        @Override
        public boolean onTouchEvent(MotionEvent event)
        {
            float x = event.getX();
            float y = event.getY();

            switch (event.getAction())
            {
                case MotionEvent.ACTION_DOWN:
                    path.moveTo(x, y);
                    path.lineTo(x, y);
                    invalidate();
                    break;
                case MotionEvent.ACTION_MOVE:
                    path.lineTo(x, y);
                    invalidate();
                    break;
            }
            return true;
        }
    }

    public int getRandomInRange(int min, int max)
    {
        return random.nextInt((max - min) + 1) + min;
    }
}
