package com.example.myfingerpaint;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity
{
    MyCanvas myCanvas;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        myCanvas = new MyCanvas(getApplicationContext());
        setContentView(myCanvas);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.my_canvas_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.itemClearCanvas:
                myCanvas.clearCanvas();
                return true;
            case R.id.itemChooseColor:
                getColorDialog().show();
                return true;
            case R.id.itemChooseWidth:
                getWidthDialog().show();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public Dialog getColorDialog()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

        builder.setMessage("Choose color");
        builder.setCancelable(true);

        builder.setPositiveButton("Red", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialogInterface, int i)
            {
                myCanvas.setLineColor(255, 0, 0);
                dialogInterface.cancel();
            }
        });

        builder.setNeutralButton("Green", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialogInterface, int i)
            {
                myCanvas.setLineColor(0, 255, 0);
                dialogInterface.cancel();
            }
        });

        builder.setNegativeButton("Blue", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialogInterface, int i)
            {
                myCanvas.setLineColor(0, 0, 255);
                dialogInterface.cancel();
            }
        });

        return builder.create();
    }

    public Dialog getWidthDialog()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

        builder.setMessage("Choose width");
        builder.setCancelable(true);

        builder.setPositiveButton("Small", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialogInterface, int i)
            {
                myCanvas.setLineWidth(10);
                dialogInterface.cancel();
            }
        });

        builder.setNeutralButton("Medium", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialogInterface, int i)
            {
                myCanvas.setLineWidth(30);
                dialogInterface.cancel();
            }
        });

        builder.setNegativeButton("Big", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialogInterface, int i)
            {
                myCanvas.setLineWidth(50);
                dialogInterface.cancel();
            }
        });

        return builder.create();
    }

    public class MyCanvas extends View
    {
        private Paint circlePaint;
        private Path circlePath;

        public void clearCanvas()
        {
            circlePath.reset();
            invalidate();
        }

        public void setLineColor(int r, int g, int b)
        {
            circlePaint.setARGB(255, r, g, b);
        }

        public void setLineWidth(int width)
        {
            circlePaint.setStrokeWidth(width);
        }

        public MyCanvas(Context context)
        {
            super(context);

            circlePath = new Path();

            circlePaint = new Paint();
            circlePaint.setARGB(255, 0, 255, 0);
            circlePaint.setStyle(Paint.Style.STROKE);
            circlePaint.setStrokeJoin(Paint.Join.ROUND);
            circlePaint.setStrokeCap(Paint.Cap.ROUND);
            circlePaint.setAntiAlias(true);
            circlePaint.setStrokeWidth(30);
        }

        @Override
        protected void onDraw(Canvas canvas)
        {
            super.onDraw(canvas);

            canvas.drawPath(circlePath, circlePaint);
        }

        @Override
        public boolean onTouchEvent(MotionEvent event)
        {
            float x = event.getX();
            float y = event.getY();

            switch (event.getAction())
            {
                case MotionEvent.ACTION_DOWN:
                    circlePath.moveTo(x, y);
                    circlePath.lineTo(x, y);
                    invalidate();
                    break;
                case MotionEvent.ACTION_MOVE:
                    circlePath.lineTo(x, y);
                    invalidate();
                    break;
            }

            return true;
        }
    }
}
