package com.example.mytesttextviewscrollapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity
{
    TextView textViewManyText;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textViewManyText = findViewById(R.id.textViewManyText);
        textViewManyText.setMovementMethod(new ScrollingMovementMethod());
        textViewManyText.setText("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec facilisis ante ac magna mattis laoreet. Maecenas ut tempor nisl. Curabitur non porttitor tellus. Sed maximus accumsan eros, nec fringilla felis ultricies ut. Mauris venenatis ac ipsum sit amet consequat. Donec placerat pellentesque ipsum eu viverra. Mauris sit amet sem sem. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Quisque congue lectus at ipsum dictum, eget dignissim eros laoreet. Phasellus elementum at erat eget ornare. Praesent vel fringilla tortor, non consequat eros. Fusce scelerisque lectus ac felis sollicitudin auctor eu ac libero.\n" +
                "\n" +
                "Curabitur ut urna tellus. Nam sed nisl euismod, egestas neque eget, aliquam odio. Phasellus id nulla ultrices, vulputate turpis non, venenatis ante. Nunc efficitur purus eget nulla pharetra lobortis. Praesent sollicitudin ipsum in felis vehicula facilisis. Nunc tempus volutpat eros, a fringilla odio vulputate a. In semper faucibus tortor nec aliquam. Nam sit amet augue dui.\n" +
                "\n" +
                "Nulla tortor magna, malesuada id nunc porttitor, faucibus faucibus justo. Curabitur vestibulum tristique ligula sit amet vestibulum. Pellentesque at est neque. Nulla tempus tortor et dui ultrices sagittis. Vivamus nec mi ac sem rutrum lacinia. Integer in arcu at neque efficitur sodales. Maecenas semper rhoncus tempor. Fusce ac augue vitae lorem condimentum commodo et et augue. Vivamus ultricies libero est, eget pulvinar dolor ornare a. Vivamus eu orci nec massa eleifend pellentesque. Fusce blandit, metus id posuere condimentum, velit nunc viverra nibh, eu luctus nunc velit id velit. Aenean scelerisque ullamcorper blandit. Aliquam erat volutpat.\n" +
                "\n" +
                "Aenean odio felis, fringilla vitae eros non, faucibus ultricies neque. Nam eleifend finibus euismod. Etiam at leo justo. Nullam sit amet mattis erat. Sed eget sapien purus. Mauris et efficitur purus. Fusce non feugiat lectus, eget dignissim eros. Proin mollis, quam non viverra bibendum, libero risus fringilla turpis, in ultrices lorem purus tristique lacus. Sed ante erat, blandit non neque eu, malesuada pretium lorem. Aliquam fermentum suscipit metus, non fermentum dui sodales at. Quisque fermentum, nisl aliquam facilisis vulputate, sem diam tempus lectus, vel facilisis enim diam vel mauris. Pellentesque fermentum odio purus, vitae rhoncus lacus congue vitae.\n" +
                "\n" +
                "Donec pulvinar ex at sem imperdiet pretium. Sed in efficitur ex, euismod convallis nunc. Duis elementum orci a arcu pretium pharetra. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Pellentesque turpis nibh, eleifend non quam at, blandit luctus urna. Proin molestie diam vitae metus tristique, at facilisis odio tempor. In at maximus velit. Quisque pretium sagittis magna et rutrum. Duis pellentesque dolor eu porta ultrices. Etiam ac enim viverra, fermentum est vitae, mattis magna. Aenean hendrerit ligula sed ipsum fringilla, id malesuada lorem lacinia. Suspendisse posuere sem eu quam sagittis bibendum. Curabitur at metus nec ante tincidunt sagittis ut eget sem. Nunc posuere cursus fringilla. Praesent at vehicula ante.");

    }
}
