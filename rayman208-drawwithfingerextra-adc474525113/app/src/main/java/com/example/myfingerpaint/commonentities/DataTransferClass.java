package com.example.myfingerpaint.commonentities;

public class DataTransferClass
{
    public static int lineWidth;
    public static int lineColorR;
    public static int lineColorG;
    public static int lineColorB;

    public enum ActivityType
    {
        ChooseColorActivity,
        ChooseWidthActivity
    }

    public static ActivityType lastActivity;
    public static boolean isCanceled;
}
