package com.example.myfingerpaint;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import com.example.myfingerpaint.commonentities.DataTransferClass;

public class ChooseLineWidthActivity extends AppCompatActivity
{
    TextView textViewLineWidth;
    SeekBar seekBarLineWidth;
    Button buttonWidthSaveAndExit, buttonWidthCancel;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_line_width);

        textViewLineWidth = findViewById(R.id.textViewLineWidth);

        seekBarLineWidth = findViewById(R.id.seekBarLineWidth);
        seekBarLineWidth.setOnSeekBarChangeListener(seekBarLineWidthOnSeek);

        buttonWidthSaveAndExit = findViewById(R.id.buttonWidthSaveAndExit);
        buttonWidthCancel = findViewById(R.id.buttonWidthCancel);

        buttonWidthSaveAndExit.setOnClickListener(buttonWidthSaveAndExitOnClick);
        buttonWidthCancel.setOnClickListener(buttonWidthCancelOnClick);
    }

    SeekBar.OnSeekBarChangeListener seekBarLineWidthOnSeek = new SeekBar.OnSeekBarChangeListener()
    {
        @Override
        public void onProgressChanged(SeekBar seekBar, int i, boolean b)
        {
            textViewLineWidth.setText("Line Width: " + seekBarLineWidth.getProgress());
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar)
        {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar)
        {

        }
    };

    View.OnClickListener buttonWidthSaveAndExitOnClick = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            DataTransferClass.lastActivity = DataTransferClass.ActivityType.ChooseWidthActivity;
            DataTransferClass.isCanceled = false;

            DataTransferClass.lineWidth = seekBarLineWidth.getProgress();

            finish();
        }
    };

    View.OnClickListener buttonWidthCancelOnClick = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            DataTransferClass.lastActivity = DataTransferClass.ActivityType.ChooseWidthActivity;
            DataTransferClass.isCanceled = true;

            finish();
        }
    };

}
