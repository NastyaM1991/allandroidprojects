package com.example.myfingerpaint;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import com.example.myfingerpaint.commonentities.DataTransferClass;

import java.util.Random;

public class ChooseColorActivity extends AppCompatActivity
{
    TextView textViewRed, textViewGreen, textViewBlue;
    TextView textViewNewColor;
    SeekBar seekBarRed, seekBarGreen, seekBarBlue;
    Button buttonSaveAndExit, buttonCancel, buttonRandomColor;

    Random random;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_color);

        random = new Random();

        textViewRed = findViewById(R.id.textViewRed);
        textViewGreen = findViewById(R.id.textViewGreen);
        textViewBlue = findViewById(R.id.textViewBlue);

        textViewNewColor = findViewById(R.id.textViewNewColor);

        seekBarRed = findViewById(R.id.seekBarRed);
        seekBarGreen = findViewById(R.id.seekBarGreen);
        seekBarBlue = findViewById(R.id.seekBarBlue);

        buttonSaveAndExit = findViewById(R.id.buttonSaveAndExit);
        buttonCancel = findViewById(R.id.buttonCancel);
        buttonRandomColor = findViewById(R.id.buttonRandomColor);

        seekBarRed.setOnSeekBarChangeListener(seekBarRedOnSeek);
        seekBarGreen.setOnSeekBarChangeListener(seekBarGreenOnSeek);
        seekBarBlue.setOnSeekBarChangeListener(seekBarBlueOnSeek);

        buttonSaveAndExit.setOnClickListener(buttonSaveAndExitOnClick);
        buttonCancel.setOnClickListener(buttonCancelOnClick);
        buttonRandomColor.setOnClickListener(buttonRandomColorOnClick);
    }

    private void showResultColor()
    {
        int r = seekBarRed.getProgress();
        int g = seekBarGreen.getProgress();
        int b = seekBarBlue.getProgress();

        textViewNewColor.setBackgroundColor(Color.argb(255, r, g, b));
    }

    SeekBar.OnSeekBarChangeListener seekBarRedOnSeek = new SeekBar.OnSeekBarChangeListener()
    {
        @Override
        public void onProgressChanged(SeekBar seekBar, int i, boolean b)
        {
            showResultColor();
            textViewRed.setText("Red: " + seekBarRed.getProgress());
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar)
        {
        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar)
        {
        }
    };

    SeekBar.OnSeekBarChangeListener seekBarGreenOnSeek = new SeekBar.OnSeekBarChangeListener()
    {
        @Override
        public void onProgressChanged(SeekBar seekBar, int i, boolean b)
        {
            showResultColor();
            textViewGreen.setText("Green: " + seekBarGreen.getProgress());
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar)
        {
        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar)
        {
        }
    };

    SeekBar.OnSeekBarChangeListener seekBarBlueOnSeek = new SeekBar.OnSeekBarChangeListener()
    {
        @Override
        public void onProgressChanged(SeekBar seekBar, int i, boolean b)
        {
            showResultColor();
            textViewBlue.setText("Blue: " + seekBarBlue.getProgress());
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar)
        {
        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar)
        {
        }
    };

    View.OnClickListener buttonSaveAndExitOnClick = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            DataTransferClass.lastActivity = DataTransferClass.ActivityType.ChooseColorActivity;
            DataTransferClass.isCanceled = false;

            DataTransferClass.lineColorR = seekBarRed.getProgress();
            DataTransferClass.lineColorG = seekBarGreen.getProgress();
            DataTransferClass.lineColorB = seekBarBlue.getProgress();

            finish();
        }
    };

    View.OnClickListener buttonCancelOnClick = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            DataTransferClass.lastActivity = DataTransferClass.ActivityType.ChooseColorActivity;
            DataTransferClass.isCanceled = true;

            finish();
        }
    };

    View.OnClickListener buttonRandomColorOnClick = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            int r = random.nextInt(255);
            int g = random.nextInt(255);
            int b = random.nextInt(255);

            seekBarRed.setProgress(r);
            seekBarGreen.setProgress(g);
            seekBarBlue.setProgress(b);

            textViewRed.setText("Red: " + seekBarRed.getProgress());
            textViewGreen.setText("Green: " + seekBarGreen.getProgress());
            textViewBlue.setText("Blue: " + seekBarBlue.getProgress());

            showResultColor();
        }
    };
}
