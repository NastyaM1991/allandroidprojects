package com.example.calculator;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.security.interfaces.DSAPublicKey;

public class MainActivity extends AppCompatActivity {
    EditText editTextFirstNumber, editTextSecondNumber, editTextResult;
    Button buttonPlus, buttonMinus, buttonMultiply, buttonDivide;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editTextFirstNumber = findViewById(R.id.editTextFirstNumber);
        editTextSecondNumber = findViewById(R.id.editTextSecondNumber);
        editTextResult = findViewById(R.id.editTextResult);

        buttonPlus = findViewById(R.id.buttonPlus);
        buttonMinus = findViewById(R.id.buttonMinus);
        buttonMultiply = findViewById(R.id.buttonMultiply);
        buttonDivide = findViewById(R.id.buttonDivide);

        buttonPlus.setOnClickListener(buttonPlusOnClick);
        buttonMinus.setOnClickListener(buttonMinusOnClick);
        buttonMultiply.setOnClickListener(buttonMultiplyOnClick);
        buttonDivide.setOnClickListener(buttonDivideOnClick);
    }

    View.OnClickListener buttonPlusOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            double firstNumber = Double.parseDouble(editTextFirstNumber.getText().toString());
            double secondNumber = Double.parseDouble(editTextSecondNumber.getText().toString());

            double result = firstNumber + secondNumber;
            editTextResult.setText(Double.toString(result));
        }
    };

    View.OnClickListener buttonMinusOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            double firstNumber = Double.parseDouble(editTextFirstNumber.getText().toString());
            double secondNumber = Double.parseDouble(editTextSecondNumber.getText().toString());

            double result = firstNumber - secondNumber;
            editTextResult.setText(Double.toString(result));
        }
    };

    View.OnClickListener buttonMultiplyOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            double firstNumber = Double.parseDouble(editTextFirstNumber.getText().toString());
            double secondNumber = Double.parseDouble(editTextSecondNumber.getText().toString());

            double result = firstNumber * secondNumber;
            editTextResult.setText(Double.toString(result));
        }
    };

    View.OnClickListener buttonDivideOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            double firstNumber = Double.parseDouble(editTextFirstNumber.getText().toString());
            double secondNumber = Double.parseDouble(editTextSecondNumber.getText().toString());

            if (secondNumber != 0) {
                double result = firstNumber / secondNumber;
                editTextResult.setText(Double.toString(result));
            } else {
                editTextResult.setText("0");
            }
        }
    };


}
