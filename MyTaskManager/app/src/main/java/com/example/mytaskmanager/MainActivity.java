package com.example.mytaskmanager;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class MainActivity extends AppCompatActivity
{
    EditText editTextDate, editTextTask;
    Button buttonAddToList, buttonClearList;
    ListView listViewTasks;
    Boolean isEditing;
    int taskIndex;

    ArrayList<TaskRecord> taskRecords;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        taskRecords = new ArrayList<>();

        editTextDate = findViewById(R.id.editTextDate);
        editTextTask = findViewById(R.id.editTextTask);

        buttonAddToList = findViewById(R.id.buttonAddToList);
        buttonClearList = findViewById(R.id.buttonClearList);

        listViewTasks = findViewById(R.id.listViewTasks);

        buttonAddToList.setOnClickListener(buttonAddToListOnClick);
        buttonClearList.setOnClickListener(buttonClearListOnClick);

        registerForContextMenu(listViewTasks);
        isEditing = false;
        taskIndex = -1;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo)
    {
        super.onCreateContextMenu(menu, v, menuInfo);

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.context_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item)
    {
        AdapterView.AdapterContextMenuInfo menuPoint = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId())
        {
            case R.id.itemEdit:
                taskIndex = menuPoint.position;
                isEditing = true;
                buttonAddToList.setText("Обновить задачу");
                buttonClearList.setEnabled(false);

                DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                editTextDate.setText(format.format(taskRecords.get(taskIndex).date));

                editTextTask.setText(taskRecords.get(taskIndex).task);


                break;
            case R.id.itemDelete:
                taskRecords.remove(menuPoint.position);
                refreshListViewTasks();
                break;
        }

        return super.onContextItemSelected(item);
    }

    View.OnClickListener buttonAddToListOnClick = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
            Date date = null;
            String task;

            try
            {
                date = format.parse(editTextDate.getText().toString());
            } catch (ParseException e)
            {
                e.printStackTrace();
            }

            task = editTextTask.getText().toString();

            if (isEditing == false)
            {
                taskRecords.add(new TaskRecord(date, task));
            }
            else
            {
                taskRecords.get(taskIndex).date = date;
                taskRecords.get(taskIndex).task = task;

                buttonClearList.setEnabled(true);
                buttonAddToList.setText("Добавить в список");
                isEditing = false;
            }

            sortTaskRecordsByDate();
            refreshListViewTasks();

            editTextDate.getText().clear();
            editTextTask.getText().clear();
        }
    };

    View.OnClickListener buttonClearListOnClick = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            taskRecords.clear();

            refreshListViewTasks();
        }
    };


    //HELPERS

    ArrayList<String> getTaskRecordsInStringList()
    {
        ArrayList list = new ArrayList();

        DateFormat format = new SimpleDateFormat("dd/MM/yyyy");

        for (int i = 0; i < taskRecords.size(); i++)
        {
            String currentString = format.format(taskRecords.get(i).date) + "\n" + taskRecords.get(i).task;
            list.add(currentString);
        }

        return list;
    }

    void refreshListViewTasks()
    {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, getTaskRecordsInStringList());

        listViewTasks.setAdapter(adapter);
    }

    void sortTaskRecordsByDate()
    {
        TaskRecord temp;
        boolean sort;
        int step = 0;

        do
        {
            sort = true;

            for (int i = 0; i < taskRecords.size() - 1 - step; i++)
            {
                if (taskRecords.get(i + 1).date.compareTo(taskRecords.get(i).date) < 0)
                {
                    temp = taskRecords.get(i);
                    taskRecords.set(i, taskRecords.get(i + 1));
                    taskRecords.set(i + 1, temp);

                    sort = false;
                }
            }
            step++;
        }
        while (!sort);
    }
}
