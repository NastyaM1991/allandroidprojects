package com.example.mytaskmanager;

import java.util.Date;

public class TaskRecord
{
    public Date date;
    public String task;

    public TaskRecord(Date date, String task)
    {
        this.date = date;
        this.task = task;
    }
}
