package com.example.lampapp;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.SeekBar;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity
{
    int red, green, blue;
    SeekBar seekBarRed, seekBarGreen, seekBarBlue;
    LinearLayout linearLayoutMy;
    Button buttonRandom, buttonFlash, buttonDisco;
    Random random;
    Boolean isRandom, flashIsOnClick, discoIsOnClick;
    Timer timer;
    TimerTask timerTask;
    int currentSeconds;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        linearLayoutMy = findViewById(R.id.linearLayoutMy);

        red = 255;
        green = 255;
        blue = 255;

        random = new Random();

        isRandom = false;

        timer = null;
        timerTask = null;
        currentSeconds = 0;
        flashIsOnClick = false;
        discoIsOnClick = false;

        seekBarRed = findViewById(R.id.seekBarRed);
        seekBarGreen = findViewById(R.id.seekBarGreen);
        seekBarBlue = findViewById(R.id.seekBarBlue);

        seekBarRed.setProgress(red);
        seekBarGreen.setProgress(green);
        seekBarBlue.setProgress(blue);

        linearLayoutMy.setBackgroundColor(Color.rgb(red, green, blue));

        seekBarRed.setOnSeekBarChangeListener(seekBarOnChange);
        seekBarGreen.setOnSeekBarChangeListener(seekBarOnChange);
        seekBarBlue.setOnSeekBarChangeListener(seekBarOnChange);

        buttonRandom = findViewById(R.id.buttonRandom);
        buttonFlash = findViewById(R.id.buttonFlash);
        buttonDisco = findViewById(R.id.buttonDisco);

        buttonRandom.setOnClickListener(buttonRandomOnClick);
        buttonFlash.setOnClickListener(buttonFlashOnClick);
        buttonDisco.setOnClickListener(buttonDiscoOnClick);
    }

    SeekBar.OnSeekBarChangeListener seekBarOnChange = new SeekBar.OnSeekBarChangeListener()
    {
        @Override
        public void onProgressChanged(SeekBar seekBar, int i, boolean b)
        {
            if (isRandom == false)
            {
                red = seekBarRed.getProgress();
                green = seekBarGreen.getProgress();
                blue = seekBarBlue.getProgress();

                linearLayoutMy.setBackgroundColor(Color.rgb(red, green, blue));
            }
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar)
        {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar)
        {

        }
    };

    View.OnClickListener buttonRandomOnClick = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            isRandom = true;

            red = random.nextInt(255);
            green = random.nextInt(255);
            blue = random.nextInt(255);

            seekBarRed.setProgress(red);
            seekBarGreen.setProgress(green);
            seekBarBlue.setProgress(blue);

            linearLayoutMy.setBackgroundColor(Color.rgb(red, green, blue));

            isRandom = false;
        }
    };

    View.OnClickListener buttonFlashOnClick = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            if (flashIsOnClick == false)
            {
                flashIsOnClick = true;

                timer = new Timer();

                timerTask = new TimerTask()
                {
                    @Override
                    public void run()
                    {
                        currentSeconds++;
                        if (currentSeconds % 2 == 0)
                        {
                            linearLayoutMy.setBackgroundColor(Color.rgb(red, green, blue));
                        }
                        else
                        {
                            linearLayoutMy.setBackgroundColor(Color.WHITE);
                        }
                    }
                };
                timer.schedule(timerTask, 1000, 500);
            }
            else
            {
                flashIsOnClick = false;
                currentSeconds = 0;

                timer.cancel();
                timer = null;
            }
        }
    };

    View.OnClickListener buttonDiscoOnClick = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            if (discoIsOnClick == false)
            {
                discoIsOnClick = true;

                timer = new Timer();

                timerTask = new TimerTask()
                {
                    @Override
                    public void run()
                    {
                        currentSeconds++;
                        if (currentSeconds % 3 == 0)
                        {
                            runOnUiThread(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    buttonRandom.performClick();
                                }
                            });
                        }
                    }
                };
                timer.schedule(timerTask, 1000, 500);
            }
            else
            {
                discoIsOnClick = false;
                currentSeconds = 0;

                timer.cancel();
                timer = null;
            }
        }
    };
}
