package com.example.drawfigures2app;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(new DrawView(getApplicationContext()));
    }

    public class DrawView extends View
    {
        Paint paint;
        Path path;
        final RectF oval;
        Rect myRect;
        RectF rect;
        Rect textBoundRect;

        float width;
        float height;
        float radius;
        float centerX, centerY;

        float xMax;
        float xMin;
        float yMax;
        float yMin;

        float rotateCenterX;
        float rotateCenterY;
        float rotateAngle;
        Point[] myPath;

        float textWidth, textHeight;
        String text;

        public DrawView(Context context)
        {
            super(context);

            paint = new Paint();
            path = new Path();
            oval = new RectF();
            myRect = new Rect();
            textBoundRect = new Rect();

            rect = new RectF();

            width = 400f;
            height = 240f;
            radius = 100f;

            centerX = 240;
            centerY = 220;

            rotateCenterX = 200;
            rotateCenterY = 200;
            rotateAngle = 45;

            text = "МЯЧ";

            myPath = new Point[]{new Point(300, 50), new Point(200, 200), new Point(200, 400), new Point(400, 400), new Point(400, 200), new Point(300, 50),};
        }

        @Override
        protected void onDraw(Canvas canvas)
        {
            super.onDraw(canvas);

            canvas.drawColor(Color.BLACK);

            //пакман
            /*path.addCircle(width, height, radius, Path.Direction.CW);

            paint.setColor(Color.WHITE);
            paint.setStrokeWidth(5);
            paint.setStyle(Paint.Style.FILL);
            paint.setAntiAlias(true);

            oval.set(centerX - radius, centerY - radius, centerX + radius, centerY + radius);
            canvas.drawArc(oval, 45, 270, true, paint);*/

            //пакман без заливки
            /*paint.setStyle(Paint.Style.STROKE);
            oval.set(centerX - 200f, centerY - 200f, centerX + 200f, centerY + 200f);
            canvas.drawArc(oval, 45, 270, true, paint);

            paint.setStyle(Paint.Style.STROKE);*/

            // разорванное кольцо
            centerY = 540;
            oval.set(centerX - radius,
                    centerY - radius,
                    centerX + radius,
                    centerY + radius);
            canvas.drawArc(oval, 135, 270, false, paint);

            paint.setStyle(Paint.Style.FILL);

            oval.set(400F, 400F, 800F, 800F);

            paint.setColor(Color.RED);
            canvas.drawArc(oval, 0F, -90F, false, paint);

            paint.setColor(Color.YELLOW);
            canvas.drawArc(oval, -90F, -90F, false, paint);

            paint.setColor(Color.GREEN);
            canvas.drawArc(oval, -180F, -90F, false, paint);

            paint.setColor(Color.BLUE);
            canvas.drawArc(oval, -270F, -90F, false, paint);

            //canvas.drawLine(0, 0, 480, 650, paint);
            paint.setColor(Color.WHITE);
            paint.setStyle(Paint.Style.STROKE);
            canvas.drawOval(0, 0, getWidth(), getHeight(), paint);

            /*paint.setStyle(Paint.Style.FILL);
            paint.setColor(Color.RED);
            canvas.drawOval(70, 70, 150, 150, paint);*/

            /*paint.setColor(Color.BLUE);
            canvas.drawOval(250, 50, 350, 300, paint);*/

            paint.setStyle(Paint.Style.FILL);

            //поворот
            canvas.rotate(-rotateAngle, rotateCenterX, rotateCenterY);

            paint.setColor(Color.BLUE);
            canvas.drawOval(250, 50, 350, 300, paint);

            canvas.rotate(rotateAngle, rotateCenterX, rotateCenterY);

            /*// закрашиваем холст жёлтым цветом
            paint.setColor(Color.YELLOW);
            // стиль Заливка
            paint.setStyle(Paint.Style.FILL);
            canvas.drawPaint(paint);*/

            //прямоугольник
            /*myRect.set(0, canvas.getHeight() / 2, canvas.getWidth(), canvas.getHeight());
            paint.setColor(Color.GREEN);
            canvas.drawRect(myRect, paint);*/

            paint.setStyle(Paint.Style.FILL);
            paint.setColor(Color.YELLOW);

            rect.set(500, 900, 900, 1300);
            canvas.drawRoundRect(rect, 50, 50, paint);

            //
            /*canvas.drawColor(Color.BLACK);

            paint.setColor(Color.WHITE);
            paint.setStrokeWidth(3);
            paint.setStyle(Paint.Style.STROKE);

            path.moveTo(myPath[0].x, myPath[0].y);

            for (int i = 1; i < myPath.length; i++)
            {
                path.lineTo(myPath[i].x, myPath[i].y);
            }

            canvas.drawPath(path, paint);*/

            //дуги
            radius = 150;

            paint.setStyle(Paint.Style.STROKE);

            centerX = 0;
            centerY = 0;

            oval.set(centerX - radius, centerY - radius, centerX + radius, centerY + radius);
            path.addArc(oval, 10, 70); // первая дуга

            oval.set(centerX - radius - 10, centerY - radius - 10, centerX + radius + 20, centerY + radius + 20);
            path.addArc(oval, 5, 80); // вторая дуга

            oval.set(centerX - radius - 20, centerY - radius - 20, centerX + radius + 40, centerY + radius + 40);
            path.addArc(oval, 0, 90); // третья дуга

            canvas.drawPath(path, paint); // выводим всё вместе

            //парашют
            path.moveTo(160.0f, 240.0f);
            path.lineTo(140.0f, 200.0f);
            path.addArc(new RectF(140, 180, 180, 220), -180, 180);
            path.lineTo(160.0f, 240.0f);
            path.close();

            canvas.drawPath(path, paint);

            //точка
            paint.setColor(Color.WHITE);
            paint.setStrokeWidth(30);
            canvas.drawPoint(200, 1000, paint);

            //текст
            /*canvas.drawColor(Color.GRAY);

            paint.setColor(Color.WHITE);
            paint.setTextSize(35.0f);
            paint.setStrokeWidth(2.0f);
            paint.setStyle(Paint.Style.STROKE);
            paint.setShadowLayer(5.0f, 10.0f, 10.0f, Color.BLACK);

            canvas.drawText("Some text...", 20, 200, paint);*/

            /*paint.setStyle(Paint.Style.FILL);
            paint.setColor(Color.YELLOW);
            canvas.drawColor(Color.WHITE);

            width = getWidth();
            height = getHeight();
            centerX = width / 2;
            centerY = height / 2;

            if (width > height)
            {
                radius = height / 4;
            }
            else
            {
                radius = width / 4;
            }

            canvas.drawCircle(centerX, centerY, radius, paint);

            paint.setColor(Color.BLUE);
            paint.setTextSize(100);

            //размер текста
            paint.getTextBounds(text, 0, text.length(), textBoundRect);

            textWidth = paint.measureText(text);
            textHeight = textBoundRect.height();

            canvas.drawText(text, centerX - (textWidth / 2f), centerY + (textHeight / 2f), paint);*/

            //график
            /*paint.setStyle(Paint.Style.STROKE);
            paint.setColor(Color.GRAY);
            paint.setAntiAlias(true);

            canvas.save();
            float xMax = 10F;
            float xMin = 0F;
            float yMax = 10F;
            float yMin = 0F;

            width = canvas.getWidth();
            height = canvas.getHeight();

            canvas.scale(width / (xMax - xMin), -height / (yMax - yMin));
            canvas.translate(-xMin + 0.2F, -yMax + 0.2F);
            paint.setStrokeWidth(.1f);

            // Ось X
            canvas.drawLine(0F, 0F, 10F, 0F, paint);

            // Ось Y
            canvas.drawLine(0F, 0F, 0F, 10F, paint);

            paint.setColor(Color.RED);
            canvas.drawLine(0F, 0F, 10F, 10F, paint);

            // Точки вдоль оси X
            canvas.drawPoint(1F, 0F, paint);
            canvas.drawPoint(2F, 0F, paint);
            canvas.drawPoint(3F, 0F, paint);

            // Точки вдоль оси Y
            canvas.drawPoint(0F, 1F, paint);
            canvas.drawPoint(0F, 2F, paint);
            canvas.drawPoint(0F, 3F, paint);

            canvas.restore();*/
        }
    }
}
