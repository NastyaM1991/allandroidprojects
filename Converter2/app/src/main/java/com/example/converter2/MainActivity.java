package com.example.converter2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import java.text.DecimalFormat;

public class MainActivity extends AppCompatActivity {

    EditText editTextMoney, editTextResult;
    Spinner spinnerCurrency;
    Button buttonConvert;

    String[] currencies = new String[]{"Dollar", "Euro", "Pounds", "Yuan"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editTextMoney = findViewById(R.id.editTextMoney);
        editTextResult = findViewById(R.id.editTextResult);

        editTextResult.setKeyListener(null);

        spinnerCurrency = findViewById(R.id.spinnerCurrency);
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, currencies);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerCurrency.setAdapter(spinnerAdapter);

        buttonConvert = findViewById(R.id.buttonConvert);
        buttonConvert.setOnClickListener(buttonConvertOnClick);
    }

    View.OnClickListener buttonConvertOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            double money = Double.parseDouble(editTextMoney.getText().toString());
            int position = spinnerCurrency.getSelectedItemPosition();

            double course = 1;

            switch (position) {
                case 0:
                    course = 73.04;
                    break;
                case 1:
                    course = 79.83;
                    break;
                case 2:
                    course = 91.03;
                    break;
                case 3:
                    course = 10.72;
                    break;
            }

            double currency = money / course;
            DecimalFormat format = new DecimalFormat("#.00");
            editTextResult.setText(format.format(currency));

        }
    };
}
