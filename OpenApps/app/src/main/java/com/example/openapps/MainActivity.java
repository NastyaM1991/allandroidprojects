package com.example.openapps;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity
{
    Button buttonCamera, buttonCall;
    private static final int CAMERA_REQUEST = 0;
    ImageView imageViewPicture;
    EditText editTextPhoneNumber;


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageViewPicture = findViewById(R.id.imageViewPicture);

        editTextPhoneNumber = findViewById(R.id.editTextPhoneNumber);

        buttonCamera = findViewById(R.id.buttonCamera);
        buttonCamera.setOnClickListener(buttonCameraOnClick);

        buttonCall = findViewById(R.id.buttonCall);
        buttonCall.setOnClickListener(buttonCallOnClick);


    }

    View.OnClickListener buttonCameraOnClick = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(cameraIntent, CAMERA_REQUEST);
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK)
        {
            Bitmap bitmap = (Bitmap) data.getExtras().get("data");
            imageViewPicture.setImageBitmap(bitmap);
        }
    }

    View.OnClickListener buttonCallOnClick = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            Intent callIntent = new Intent(Intent.ACTION_DIAL);
            callIntent.setData(Uri.parse("tel:" + editTextPhoneNumber.getText().toString()));
            startActivity(callIntent);
        }
    };
}
