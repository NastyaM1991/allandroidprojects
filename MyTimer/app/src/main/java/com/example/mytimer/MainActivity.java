package com.example.mytimer;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity
{
    TextView textViewTime;
    Button buttonStart, buttonPause, buttonReset, buttonRecordTime, buttonClearListTimeRecords;
    ListView listViewTimeRecords;
    Timer timer;
    TimerTask timerTask;

    ArrayList<String> listTimeRecords;

    int currentSeconds;

    boolean isOnPause;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textViewTime = findViewById(R.id.textViewTime);

        buttonStart = findViewById(R.id.buttonStart);
        buttonPause = findViewById(R.id.buttonPause);
        buttonReset = findViewById(R.id.buttonReset);
        buttonRecordTime = findViewById(R.id.buttonRecordTime);
        buttonClearListTimeRecords = findViewById(R.id.buttonClearListTimeRecords);

        listViewTimeRecords = findViewById(R.id.listViewTimeRecords);

        currentSeconds = 0;
        timer = null;
        timerTask = null;

        listTimeRecords = new ArrayList<>();

        isOnPause = false;

        buttonPause.setEnabled(false);
        buttonReset.setEnabled(false);

        buttonStart.setOnClickListener(buttonStartOnClick);
        buttonPause.setOnClickListener(buttonPauseOnClick);
        buttonReset.setOnClickListener(buttonResetOnClick);
        buttonRecordTime.setOnClickListener(buttonRecordTimeOnClick);
        buttonClearListTimeRecords.setOnClickListener(buttonClearListTimeRecordsOnClick);
    }

    View.OnClickListener buttonStartOnClick = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            if (isOnPause == false)
            {
                timer = new Timer();

                timerTask = new TimerTask()
                {
                    @Override
                    public void run()
                    {
                        if (isOnPause == true)
                        {
                            return;
                        }
                        currentSeconds++;
                        int sec = currentSeconds % 60;
                        int min = currentSeconds / 60;
                        int hours = 0;

                        if (min > 59)
                        {
                            hours = min / 60;
                            min = min % 60;
                        }

                        final String output = String.format("%02d:%02d:%02d", hours, min, sec);
                        textViewTime.setText(output);
                        /*runOnUiThread(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                textViewTime.setText(output);
                            }
                        });*/
                    }
                };

                timer.schedule(timerTask, 1000, 1000);

                buttonStart.setEnabled(false);
                buttonPause.setEnabled(true);
                buttonReset.setEnabled(true);
            }
            else
            {
                isOnPause = false;

                buttonStart.setEnabled(false);
                buttonPause.setEnabled(true);
                buttonReset.setEnabled(true);
            }
        }
    };

    View.OnClickListener buttonPauseOnClick = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            isOnPause = true;

            buttonStart.setEnabled(true);
            buttonPause.setEnabled(false);
            buttonReset.setEnabled(true);
        }
    };

    View.OnClickListener buttonResetOnClick = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            currentSeconds = 0;
            isOnPause = false;

            timer.cancel();
            timer = null;

            textViewTime.setText("00:00:00");

            buttonStart.setEnabled(true);
            buttonPause.setEnabled(false);
            buttonReset.setEnabled(false);
        }
    };

    View.OnClickListener buttonRecordTimeOnClick = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            listTimeRecords.add(textViewTime.getText().toString());

            ArrayAdapter<String> adapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_list_item_1, listTimeRecords);

            listViewTimeRecords.setAdapter(adapter);
        }
    };

    View.OnClickListener buttonClearListTimeRecordsOnClick = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            listTimeRecords.clear();

            ArrayAdapter<String> adapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_list_item_1, listTimeRecords);

            listViewTimeRecords.setAdapter(adapter);
        }
    };
}
