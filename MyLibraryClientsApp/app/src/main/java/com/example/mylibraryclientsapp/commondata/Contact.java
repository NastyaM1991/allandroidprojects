package com.example.mylibraryclientsapp.commondata;

import java.util.Date;

public class Contact
{
    public String name;
    public String bookName;
    public Date handingDate;

    public Contact(String name, String bookName, Date handingDate)
    {
        this.name = name;
        this.bookName = bookName;
        this.handingDate = handingDate;
    }
}
