package com.example.mylibraryclientsapp.commondata;

import java.util.ArrayList;

public class ContactsList
{
    private ArrayList<Contact> contacts;

    public ContactsList()
    {
        contacts = new ArrayList<>();
    }

    public void add(Contact contact)
    {
        contacts.add(contact);
    }

    public ArrayList<String> getContactsInString()
    {
        ArrayList<String> strings = new ArrayList<>();

        for (int i = 0; i < contacts.size(); i++)
        {
            String output = "Name: " + contacts.get(i).name + "\n" + "Book name: " + contacts.get(i).bookName + "\n" + "Handing date: " + contacts.get(i).handingDate + "\n";

            strings.add(output);
        }

        return strings;
    }
}
