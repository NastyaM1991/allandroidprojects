package com.example.mylibraryclientsapp;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.example.mylibraryclientsapp.commondata.Contact;
import com.example.mylibraryclientsapp.commondata.DataTransferClass;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ContactAddActivity extends AppCompatActivity
{
    EditText editTextName, editTextBookName, editTextHandingDate;
    Button buttonAddAndClose;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_add);

        editTextName = findViewById(R.id.editTextName);
        editTextBookName = findViewById(R.id.editTextBookName);

        editTextHandingDate = findViewById(R.id.editTextHandingDate);

        buttonAddAndClose = findViewById(R.id.buttonAddAndClose);
        buttonAddAndClose.setOnClickListener(buttonAddAndCloseOnClick);

    }

    View.OnClickListener buttonAddAndCloseOnClick = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            String name = editTextName.getText().toString();
            String bookName = editTextBookName.getText().toString();

            DateFormat format = new SimpleDateFormat("dd/MM/yyyy");

            Date handingDate = null;

            try
            {
                handingDate = format.parse(editTextHandingDate.getText().toString());
            } catch (ParseException e)
            {
                e.printStackTrace();
            }

            DataTransferClass.contactsList.add(new Contact(name, bookName, handingDate));

            finish();
        }
    };
}
